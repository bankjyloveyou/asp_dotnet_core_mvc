﻿using Microsoft.EntityFrameworkCore;
using WorkShop.Models;


namespace WorkShop.Data
{
    public class MvcBookContext : DbContext
    {
        public MvcBookContext (DbContextOptions options)
            :base (options)
        {
        }
        public DbSet<Book> Book { get; set; }

    }
}
